import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Casino {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Crupie crupie = new Crupie();
//		Jogador jogador = new Jogador();
		ArrayList<Jogador> jogadores; 

		Carta carta;
		String mensagem;
		int pontos = 0;
		int qtdeJogadores;
		
		System.out.println("Bem vindo ao Cassino Mastertech \n");
		System.out.println("Quantas pessoas querem jogar 21? ");
		
		qtdeJogadores = scanner.nextInt();
		jogadores = new ArrayList<Jogador>();
		
		for (int i=0; i<qtdeJogadores; i++) {
			jogadores.add(new Jogador());
		}
		
		do {
			for (Jogador jogadorAtual: jogadores) {
				carta = crupie.novaCarta();
	
				mensagem = jogadorAtual.receberCarta(carta);
		   		pontos = jogadorAtual.getPontos();
		   		
		   		System.out.println(mensagem);	
	
			}
		}while (crupie.validarRodada(pontos));
	}
}
