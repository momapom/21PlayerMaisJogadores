import java.util.List;

public class Crupie {
	private Baralho baralho;

	public Crupie() {
		this.baralho = new Baralho();
	}
	
	public Carta novaCarta() {
		Carta novaCarta;
		novaCarta = this.baralho.getCarta();
		if (novaCarta==null) {
			System.out.println("Sorry, acabaram as cartas");
			return null;	
		} 
		return novaCarta;
	}
	
	public boolean validarRodada(int pontos){
		if (pontos == 21) {
			System.out.println("Você ganhou o 21!!"); 
			return false;
		} else if (pontos > 21) {
			System.out.println("Já era, você perdeu..");
			return false; 
		} 
		return true;
	}
}   

